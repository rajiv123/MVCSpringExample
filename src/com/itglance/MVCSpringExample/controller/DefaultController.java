package com.itglance.MVCSpringExample.controller;


import com.itglance.MVCSpringExample.dao.CustomerDAO;
import com.itglance.MVCSpringExample.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;

import java.sql.SQLException;
import java.util.List;


@Controller
@RequestMapping("/")
public class DefaultController {

    @Autowired
    private CustomerDAO customerDAO;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        try {
            List<Customer> customerList = customerDAO.getAll();
            model.addAttribute("customerList", customerList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "index";
    }

    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public String insert() {
        return "insert";
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("customer") Customer customer, Model model) throws SQLException, ClassNotFoundException {
        customerDAO.insert(customer);
        model.addAttribute("customerList", customerDAO.getAll());

        return "index";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String delete( int id, Model model) throws SQLException, ClassNotFoundException {
        System.out.println("hello");
        System.out.println(id);
        customerDAO.delete(id);
        model.addAttribute("customerList", customerDAO.getAll());
        return "index";
    }


    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String update(int id, Model model) {

        try {
            model.addAttribute(customerDAO.getRecordById(id));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return "update";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)

    public String update(@ModelAttribute("customer") Customer customer, Model model) {
        System.out.println(customer.getFirstName());
        System.out.println(customer.getAge());
        System.out.println("hello");
        try {
            customerDAO.update(customer);
            model.addAttribute("customerList", customerDAO.getAll());
        } catch (Exception e) {
        }
        return "index";
    }


}