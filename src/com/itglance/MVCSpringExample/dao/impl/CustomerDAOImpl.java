package com.itglance.MVCSpringExample.dao.impl;

import com.itglance.MVCSpringExample.dao.CustomerDAO;
import com.itglance.MVCSpringExample.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository(value="customerDAO")

public class CustomerDAOImpl implements CustomerDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public List<Customer> getAll() throws ClassNotFoundException, SQLException {
        String sql = "SELECT * FROM customers";

        List<Customer> customerList = jdbcTemplate.query(sql, new RowMapper<Customer>() {
            @Override
            public Customer mapRow(ResultSet resultSet, int i) throws SQLException {
                Customer customer = new Customer();
                customer.setFirstName(resultSet.getString("first_name"));
                customer.setId(resultSet.getInt("id"));
                customer.setLastName(resultSet.getString("last_name"));
                customer.setAge(resultSet.getInt("age"));
                return customer;
            }
        });

        return customerList;
    }

    @Override
    public int insert(Customer customer) throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO customers(first_name, last_name, age) values(?,?,?)";
        return jdbcTemplate.update(sql, customer.getFirstName(), customer.getLastName(), customer.getAge());
    }

    @Override
    public int delete(int id) throws ClassNotFoundException, SQLException {
        System.out.println(id);
        String sql="DELETE from customers where id=?";
        return  jdbcTemplate.update(sql,id);
    }

    @Override
    public int update(Customer customer) throws ClassNotFoundException, SQLException {
        String sql="UPDATE customers SET first_name=?,last_name=?,age=? where id=?";
        System.out.println(customer.getFirstName());
        return jdbcTemplate.update(sql,customer.getFirstName(),customer.getLastName(),customer.getAge(),customer.getId());

    }

    public Customer getRecordById(int id) throws ClassNotFoundException, SQLException {
        String sql="Select * from customers where id=" + id;
        return jdbcTemplate.query(sql, new ResultSetExtractor<Customer>() {
            @Override
            public Customer extractData(ResultSet rs) throws SQLException, DataAccessException {

                if(rs.next()){
                    Customer customer=new Customer();
                    customer.setFirstName(rs.getString("first_name"));
                    customer.setLastName(rs.getString("last_name"));
                    customer.setAge(rs.getInt("age"));
                    customer.setId(rs.getInt("id"));
                    return customer;

                }
                return null;
            }
        });


    }

}
